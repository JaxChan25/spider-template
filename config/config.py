#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

class Config:

    logging_dir=os.path.join(os.path.dirname(__file__),'../logs')
    log_config = dict(
        handlers=[
            dict(sink=os.path.join(logging_dir,"info.log"),
                 # serialize=True,
                 format="[{time}] {message}",
                 level='INFO',
                 colorize=True,
                 rotation='1 day',
                 retention='7 day',
                 encoding='utf-8',
                 ),
            dict(sink=sys.stderr,
                 # serialize=True,
                 format="[{time}] {message}",
                 level='INFO',
                 colorize=True,
                 ),

            dict(sink=os.path.join(logging_dir,"error.log"),
                 # serialize=True,
                 format="[{time}] {message}",
                 level='WARNING',
                 colorize=True,
                 rotation='1 day',
                 retention='7 day',
                 encoding='utf-8',
                 ),
        ]
    )
