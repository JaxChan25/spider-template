# Sample
本项目是Python 数据采集项目结构化的模板，目录结构仅供参考，根据项目实际情况进行调整。

建议在`sample/modules`文件夹下去初始化并使用以下框架（**后面如果发现更好的会持续补充说明**）。

**这里我建议是轻量采集用下面的feapder-AirSpider，一个函数就可以满足需求。重量采集使用scarpy或者feapder-Spider**

1. [feapder,秉承着开发快速、抓取快速、简单、轻量且功能强大的原则](http://boris.org.cn/feapder/#/) 。

   1. [AirSpider](http://boris.org.cn/feapder/#/usage/AirSpider?id=airspider) ，对应文件夹的`my_airspider`. 面对一些数据量较少，无需断点续爬，无需分布式采集的需求，可采用此爬虫。以下列出使用airspider需要的配置：
      1. 配置MONGO作为存储数据库
   2. [Spider](http://boris.org.cn/feapder/#/usage/Spider?id=spider) ，分布式爬虫，适用于海量数据采集，支持断点续爬、爬虫报警、数据自动入库等功能。以下列出使用spider需要的配置：
      1. 配置redis作为分布式中间件
      2. 配置MONGO作为存储数据库

2. [scarpy](https://scrapy-chs.readthedocs.io/zh_CN/0.24/intro/tutorial.html) 分布式重量采集

   

## python版本要求

统一使用py3.7版本



## 目录结构

```
├── config                    # 配置文件目录
├── data                      # 数据文件存放目录
├── docker                    # 构建docker镜像的目录
├── docs                      # 放置项目文档的目录，如接口api文档、需求文档等
├── logs                      # 放置日志文件的目录
├── models                    # 模型文件存放目录
├── sample                    # 项目主体代码目录，该目录下的子目录仅供参考，根据实际情况调整
│  ├── __init__.py            # 主体代码的入口
│  ├── log                    # 日志对象
│  ├── modules                # 可复用模块目录
│  ├── scripts                # 常用脚本目录，如直接调用主体代码脚本等。
│  ├── service                # 提供主体代码的RESTFUL调用服务，服务框架为fastapi
│  │  ├── app.py              # flask的主入口文件
│  ├── tests                  # 测试代码的目录
│  ├── utils                  # 常用工具函数目录
│  ├── ....                   
├── .gitignore                # git忽略追踪文件
├── LICENSE                   # 开源许可证明
├── requirements.txt          # 依赖文件
├── README.md                 # read me 文件
```

注：需要将项目名称sample和目录中的sample都改成相应的模块名称



## 日志

- 项目统一使用loguru库作为日志库，使用单例保证整个模块使用同一个日志对象。  
- 默认配置下INFO级别以下的日志会放在`logs/info.log`文件下，WARNING级别以上的日志会放在`logs/error.log`文件下，并输出到
  stderr中，可在`config/config.py`中修改配置  
- 日志基本使用方法：

```python3
from sample.log import logger

logger.info("Hello.")
```

- 其他用法请查看loguru文档：`https://loguru.readthedocs.io/en/stable/index.html`





## （选用）FastApi服务启动方法

- 项目统一使用Flask作为后台框架
- 启动服务：进入sample目录下，使用以下命令
```
$ flask run
# 启动后，服务就部署在http://127.0.0.1:5000上
```





## 部署和交付方式
- 代码统一部署到docker容器中
- 使用Dockerfile构建容器
- 使用docker-compose启动容器