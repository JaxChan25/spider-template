#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .logger import get_logger

#单例
__all__ =['logger']

logger = get_logger()